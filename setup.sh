#!/bin/bash
set -x

PATH_BIN="/usr/local/bin/tanker"

cp tanker.service tanker.socket /etc/systemd/system/
mkdir -p ${PATH_BIN}
cp -R src/static src/templates/ src/*.py ${PATH_BIN}
pip3 install -r requirements.txt
systemctl daemon-reload
systemctl enable tanker.service
systemctl start tanker.service


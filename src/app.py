from pathlib import Path

from flask import Flask, render_template, jsonify, url_for

from config import Config

app = Flask(__name__)
app.config.from_object(Config)

states = {
    'unbroken': 'sea.jpg',
    'broken': 'oil.gif'
}

DEFAULT_STATE = 'unbroken'


def get_img():
    file = Path(app.config['FILE_PATH'])

    if not file.exists():
        with file.open('w') as f:
            f.write(DEFAULT_STATE)

    with open(app.config['FILE_PATH'], 'r') as f:
        state = f.read().strip()

    path = states.get(state, 'sea.jpg')
    return path


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', path=get_img())


@app.route('/img_path')
def img_path():
    return jsonify({'path': url_for('static', filename=get_img())})


if __name__ == '__main__':
    app.run()

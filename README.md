# Tanker service

##Зависимости
* python 3.6
* все указаное в requirements.txt
* systemd

## Запуск
```bash
sudo setup.sh
```

Создается сервис systemd `tanker.service`

Картинки залить в src/static
Если используются другие названия, то изменить в файле src/app.py `states`

Путь до файла указан в src/config `FILE_PATH`

Сейчас понимается два состояния `broken` и `unbroken`.
